const mongoose = require('mongoose')
const {Schema}=mongoose

const AcronymSchema= new Schema({
    acronym:{type:String,required:true,},
    description:{type:String, required:true},
},{
    timestamps:{
        createdAt:true,updatedAt:true
    }
})

module.exports= mongoose.model('acronym',AcronymSchema)