const {Router}=require('express')
const {AuthMiddleware} = require('../middlewares')

module.exports= function({
      AcronymController
}){
    const router = Router();
    //GET /acronym?from=50&limit=10&search=:search
    router.get('',AcronymController.getPaginated);

    //GET /acronym/:acronym
    router.get('/:acronym',AcronymController.get);
    //PUT /acronym/:acronym
    router.put('/:acronym',AuthMiddleware,AcronymController.update);
    //DELETE /acronym/:acronym
    router.delete('/:acronym',AuthMiddleware,AcronymController.delete);
    //POST /acronym
    router.post('/',AcronymController.add);

    return router;
}