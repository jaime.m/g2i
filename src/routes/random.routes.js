const {Router}=require('express')

module.exports= function({
 AcronymController
}){
    const router = Router();
    //GET /random/:count?
    router.get('/:count?',AcronymController.getRandom);
    return router;
}