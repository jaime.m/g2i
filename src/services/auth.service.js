const {generateToken} = require('../helpers/jwt.helper');

class AuthService {
    
    async singin(){
        const token=generateToken();
        return {
            token,          
        }
    }
}

module.exports=AuthService;