const BaseService = require('./base.service')
let _acronymRepository=null

class AcronymService extends BaseService{
    constructor({
         AcronymRepository
    }){
        super(AcronymRepository)
        _acronymRepository=AcronymRepository
    }
    async update(description,acronym){
        
        const exist= await _acronymRepository.get(acronym);

        if(!exist) {
            const error = new Error();
            error.status=400;
            error.message="Acronym does not exist in database";
            throw error;
        }
        const updatedAcronym= await _acronymRepository.update(description,acronym)
        return updatedAcronym;  

    }
    async delete(acronym){

        const exist= await _acronymRepository.get(acronym);
        if(!exist) {
            const error = new Error();
            error.status=400;
            error.message="Acronym does not exist in database";
            throw error;
        }

        return  await this.repository.delete(acronym);
    }


}

module.exports=AcronymService