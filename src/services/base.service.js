class BaseService{
    constructor(repository){
        this.repository=repository
    }
    
    async get(name){
        if(!name) {
            const error = new Error()
            error.status=400
            error.message="Id must be send"
            throw error
        }  

        const currentlyEntity= await this.repository.get(name)
        if(!name) {
            const error = new Error()
            error.status=400
            error.message="Id must be send"
            throw error
        }
        if(!currentlyEntity) {
            const error = new Error()
            error.status=400
            error.message="Acronym not found"
            throw error
        }

        return currentlyEntity  
    }

    async getRandom(count=1){
        const currentlyEntity= await this.repository.getRandom(count);
        return currentlyEntity ;
    }
    async getPaginated(from=0, limit=10,search=''){

        if(isNaN(from) && isNaN(limit)) {
            const error = new Error()
            error.status=400
            error.message="from and limit must be numbers"
            throw error
        }
        const currentlyEntity= await this.repository.getPaginated(from, limit,search);
        return currentlyEntity;
    }
    async create(entity){ 
            let newEntity = await this.repository.create(entity);
            return newEntity;
            
    }

}

module.exports=BaseService