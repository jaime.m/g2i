const {asClass,asFunction,asValue,createContainer} = require('awilix')
//config
const config=require('../config')
const app = require('.')
//Routes
const {AcronymRoutes,RandomRoutes, AuthRoutes,} = require('../routes/index.routes')
const Routes = require('../routes')
//Controllers
const { AcronymController,AuthController,}=require('../controller')
//Services
const {AcronymService, AuthService,} = require('../services')
//Repositories
const {AcronymRepository,}= require('../repositories')
//Models
const {AcronymModel, } =require('../models')

const container= createContainer()

container.register({
    router:asFunction(Routes).singleton(),
    app:asClass(app).singleton(),
    config:asValue(config)
})
//Registering the models
.register({
    AcronymModel:asValue(AcronymModel),
})
//Registering the Services
.register({
    AcronymService:asClass(AcronymService).singleton(),
    AuthService:asClass(AuthService).singleton(),
})
//Registering the Repositories
.register({
    AcronymRepository:asClass(AcronymRepository).singleton(),
})
.register({
    AcronymRoutes:asFunction(AcronymRoutes).singleton(),
    RandomRoutes:asFunction(RandomRoutes).singleton(),
    AuthRoutes:asFunction(AuthRoutes).singleton(),
})
.register({
    AcronymController:asClass(AcronymController.bind(AcronymController)).singleton(),
    AuthController:asClass(AuthController.bind(AuthController)).singleton(),
})

module.exports=container