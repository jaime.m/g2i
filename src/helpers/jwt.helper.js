const {sign} = require ('jsonwebtoken')
const {JWT_SECRET}=require('../config')

module.exports.generateToken = function(){
    return sign ({allowed:true},JWT_SECRET,{expiresIn:"4h"})
}