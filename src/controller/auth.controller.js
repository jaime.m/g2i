
let _authService=null

class AuthController{
 constructor({AuthService}){
    _authService=AuthService
 }

 async singin(req, res){
    const creds= await _authService.singin()
    return res.status(200).send(creds)
 }
}
module.exports=AuthController