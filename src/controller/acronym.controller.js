let _acronymService=null
class AcronymController{
    constructor({AcronymService}){
        _acronymService=AcronymService
    }
    async get(req,res){
        const {acronym}=req.params;
        const result = await _acronymService.get(acronym);
        return res.send(result);
    }
    async getRandom(req,res){
        const count=req.params.count || 1;
        if(isNaN(count)){
            const error = new Error();
            error.status=400;
            error.message="count must be a number";
            throw error;
        }
        const results = await _acronymService.getRandom(count);
        return res.send(results);
    }

    async getPaginated(req,res){
        const {from, limit,search}=req.query;
        const results = await _acronymService.getPaginated(from, limit,search);
        return res.send(results);
    }

    async delete(req,res){
        const {acronym}=req.params;
        const deletesAcronym = await _acronymService.delete(acronym);
        return res.send(deletesAcronym);
    }

    async update(req,res){
        const {description}=req.body
        const {acronym}=req.params;

        if(!acronym) {
            const error = new Error()
            error.status=400
            error.message="acronym must be send"
            throw error
        }  
        if(!description) {
            const error = new Error()
            error.status=400
            error.message="description must be send"
            throw error
        }
        const updatedAcronym = await _acronymService.update(description,acronym)
        return res.send(updatedAcronym);
    }

    async add(req,res){
        const body=req.body
        const {acronym,description}=body;
        if(!acronym) {
            const error = new Error()
            error.status=400
            error.message="acronym must be send"
            throw error
        }  
        if(!description) {
            const error = new Error()
            error.status=400
            error.message="description must be send"
            throw error
        }  
        const newAcronym = await _acronymService.create(body)
        return res.send(newAcronym)
    }

    
}
module.exports=AcronymController
