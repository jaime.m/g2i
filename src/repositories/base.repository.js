
class BaseRepository{
    constructor(model){
        this.model=model
    }

    async create(entity){
     
        let ca = await this.model.create(entity)
       
        return ca;
    }
    

    async delete(id,entity){
      
        return await this.model.findByIdAndUpdate(id,{active:false},{new:true})
    }
}

module.exports=BaseRepository