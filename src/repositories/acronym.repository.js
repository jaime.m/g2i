const BaseRepository= require('./base.repository')
let _acronym=null

class AcronymRepository extends BaseRepository{
    constructor({AcronymModel}){
        super(AcronymModel)
        _acronym=AcronymModel;
    }

    async get(acronym){
        const entity=await _acronym.findOne({acronym});
        return entity;
    }
    async getRandom(count){
        const entity=await _acronym.aggregate([{ $sample: { size: Number(count) } }]);
        return entity;
    }
    async getPaginated(from, limit,search){

        limit=Number(limit);
        from=Number(from);
        const collection=await _acronym
        .find({ 'acronym': { $regex: search }})
        .skip(limit * from)
        .limit(limit);

        const quantity =await _acronym
        .find({ 'acronym': { $regex: search }})
        .count();
        let moreResults= quantity>limit*from+limit;
        return {collection,moreResults};
    }
    async update(description,acronym){
        
        let updated=await _acronym.findOneAndUpdate({acronym},{description},{new:true}); 
        return updated
    }
    async delete(acronym){
        return await this.model.findOneAndDelete({acronym});
    }
}

module.exports=AcronymRepository;