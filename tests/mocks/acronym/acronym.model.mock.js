const mongoose =require('mongoose');

module.exports={
    acronym:{
        _id:mongoose.Types.ObjectId(),
        acronym:'ilu',
        description:'I love you'
    },
    acronyms:[
        {
            _id:mongoose.Types.ObjectId(),
            acronym:'ilu',
            description:'I love you'
        },
        {
            _id:mongoose.Types.ObjectId(),
            acronym:'ilu2m',
            description:'I love you too much'
        }
    ]
};