module.exports={
    get: jest.fn(),
    getRandom:jest.fn(),
    getPaginated:jest.fn(),
    update:jest.fn(),
    delete:jest.fn(),
    create:jest.fn(),
}