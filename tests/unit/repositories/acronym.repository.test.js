const {AcronymRepository} =require('../../../src/repositories');
const {AcronymModel} =require('../../../src/models');
const mockingoose  =require('mockingoose').default;
let {AcronymModelMock:{acronym,acronyms}} = require('../../mocks');
const acronymModel = require('../../../src/models/acronym.model');

describe('Acronym repository test',()=>{
    beforeEach(()=>{
        mockingoose.resetAll();
        jest.clearAllMocks();
    });

    it('Should return a Acronym with describtion by acronym name',async ()=>{
        const _acronym = {...acronym};
        mockingoose(acronymModel).toReturn(acronym,'findOne');
        const _acronymRepository = new AcronymRepository({AcronymModel});
        const expected = await _acronymRepository.get(_acronym.acronym);
         expect(JSON.parse(JSON.stringify(expected))).toHaveProperty('acronym',_acronym.acronym);
         expect(JSON.parse(JSON.stringify(expected))).toHaveProperty('description',_acronym.description);
    })
    it('Should return a random  Acronym',async ()=>{
        const _acronym = {...acronym};
        mockingoose(acronymModel).toReturn(acronyms,'aggregate');
        const _acronymRepository = new AcronymRepository({AcronymModel});
        const expected = await _acronymRepository.getRandom(2);
        expect(expected.length).toBe(2);
    })
    
    it('Should return a Acronym with describtion by acronym name',async ()=>{
        const _acronym = {...acronym};
        mockingoose(acronymModel).toReturn(acronyms,'aggregate');
        const _acronymRepository = new AcronymRepository({AcronymModel});
        const expected = await _acronymRepository.getRandom(2);
        expect(expected.length).toBe(2);
    })
})